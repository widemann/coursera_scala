These are the homework assignments for:

**Functional Programming Principles in Scala** by **Martin Odersky**

https://class.coursera.org/progfun-005

Week: Project Name 

 - week_0: example  
 - week_1: recfun  
 - week_2: funsets
