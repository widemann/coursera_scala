
package objsets

object tmp {
  println("Welcome to the Scala worksheet")       //> Welcome to the Scala worksheet
  var a = new Tweet("a","a_text",45)              //> a  : objsets.Tweet = User: a
                                                  //| Text: a_text [45]
  a.toString                                      //> res0: String = User: a
                                                  //| Text: a_text [45]
  var b = new Empty                               //> b  : objsets.Empty = objsets.Empty@6442b0a6

  def f(t: Tweet) = {
  	println("retweets = " + t.retweets)
  	//println(t.toString)
  }                                               //> f: (t: objsets.Tweet)Unit
    
    val set1 = new Empty                          //> set1  : objsets.Empty = objsets.Empty@60f82f98
    val set2 = set1.incl(new Tweet("a", "a body", 20))
                                                  //> set2  : objsets.TweetSet = objsets.NonEmpty@35f983a6
    val set3 = set2.incl(new Tweet("b", "b body", 20))
                                                  //> set3  : objsets.TweetSet = objsets.NonEmpty@7f690630
    val c = new Tweet("c", "c body", 7)           //> c  : objsets.Tweet = User: c
                                                  //| Text: c body [7]
    val d = new Tweet("d", "d body", 9)           //> d  : objsets.Tweet = User: d
                                                  //| Text: d body [9]
    val set4c = set3.incl(c)                      //> set4c  : objsets.TweetSet = objsets.NonEmpty@edf4efb
    val set4d = set3.incl(d)                      //> set4d  : objsets.TweetSet = objsets.NonEmpty@2f7a2457
    val set5 = set4c.incl(d)                      //> set5  : objsets.TweetSet = objsets.NonEmpty@566776ad
    
   def asSet(tweets: TweetSet): Set[Tweet] = {
    var res = Set[Tweet]()
    tweets.foreach(res += _)
    res
  }                                               //> asSet: (tweets: objsets.TweetSet)Set[objsets.Tweet]

  def size(set: TweetSet): Int = asSet(set).size  //> size: (set: objsets.TweetSet)Int
    
   val s = size(set5)                             //> s  : Int = 4
   
   set5.foreach(f)                                //> retweets = 20
                                                  //| retweets = 20
                                                  //| retweets = 7
                                                  //| retweets = 9
   
   
   //set1.mostRetweeted
   

	def g(t: Tweet) = {println("retweets = " + t.retweets)}
                                                  //> g: (t: objsets.Tweet)Unit
	set5.foreach(g)                           //> retweets = 20
                                                  //| retweets = 20
                                                  //| retweets = 7
                                                  //| retweets = 9
  g(c)                                            //> retweets = 7

  //set5.mostRetweeted
 
  var alist = List(1,2,3)                         //> alist  : List[Int] = List(1, 2, 3)
  var blist = alist::List(4)                      //> blist  : List[Any] = List(List(1, 2, 3), 4)
  var blist2 = alist ++ List(4)                   //> blist2  : List[Int] = List(1, 2, 3, 4)
  var mx_alist = alist.max                        //> mx_alist  : Int = 3
  
  
  //var reL = set5.retweetList
  
  
  
  
  
}