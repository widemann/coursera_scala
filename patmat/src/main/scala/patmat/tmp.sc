package patmat
import patmat.Huffman._

object tmp {
  println("Welcome to the Scala worksheet")       //> Welcome to the Scala worksheet
    //abstract class CodeTree
    val t1 = Fork(Leaf('a',2), Leaf('b',3), List('a','b'), 5)
                                                  //> t1  : patmat.Huffman.Fork = Fork(Leaf(a,2),Leaf(b,3),List(a, b),5)
    val t2 = Fork(Fork(Leaf('a',2), Leaf('b',3), List('a','b'), 5), Leaf('d',4), List('a','b','d'), 9)
                                                  //> t2  : patmat.Huffman.Fork = Fork(Fork(Leaf(a,2),Leaf(b,3),List(a, b),5),Leaf
                                                  //| (d,4),List(a, b, d),9)

  
  val frenchCode: CodeTree = Fork(Fork(Fork(Leaf('s',121895),Fork(Leaf('d',56269),Fork(Fork(Fork(Leaf('x',5928),Leaf('j',8351),List('x','j'),14279),Leaf('f',16351),List('x','j','f'),30630),Fork(Fork(Fork(Fork(Leaf('z',2093),Fork(Leaf('k',745),Leaf('w',1747),List('k','w'),2492),List('z','k','w'),4585),Leaf('y',4725),List('z','k','w','y'),9310),Leaf('h',11298),List('z','k','w','y','h'),20608),Leaf('q',20889),List('z','k','w','y','h','q'),41497),List('x','j','f','z','k','w','y','h','q'),72127),List('d','x','j','f','z','k','w','y','h','q'),128396),List('s','d','x','j','f','z','k','w','y','h','q'),250291),Fork(Fork(Leaf('o',82762),Leaf('l',83668),List('o','l'),166430),Fork(Fork(Leaf('m',45521),Leaf('p',46335),List('m','p'),91856),Leaf('u',96785),List('m','p','u'),188641),List('o','l','m','p','u'),355071),List('s','d','x','j','f','z','k','w','y','h','q','o','l','m','p','u'),605362),Fork(Fork(Fork(Leaf('r',100500),Fork(Leaf('c',50003),Fork(Leaf('v',24975),Fork(Leaf('g',13288),Leaf('b',13822),List('g','b'),27110),List('v','g','b'),52085),List('c','v','g','b'),102088),List('r','c','v','g','b'),202588),Fork(Leaf('n',108812),Leaf('t',111103),List('n','t'),219915),List('r','c','v','g','b','n','t'),422503),Fork(Leaf('e',225947),Fork(Leaf('i',115465),Leaf('a',117110),List('i','a'),232575),List('e','i','a'),458522),List('r','c','v','g','b','n','t','e','i','a'),881025),List('s','d','x','j','f','z','k','w','y','h','q','o','l','m','p','u','r','c','v','g','b','n','t','e','i','a'),1486387)
                                                  //> frenchCode  : patmat.Huffman.CodeTree = Fork(Fork(Fork(Leaf(s,121895),Fork(
                                                  //| Leaf(d,56269),Fork(Fork(Fork(Leaf(x,5928),Leaf(j,8351),List(x, j),14279),Le
                                                  //| af(f,16351),List(x, j, f),30630),Fork(Fork(Fork(Fork(Leaf(z,2093),Fork(Leaf
                                                  //| (k,745),Leaf(w,1747),List(k, w),2492),List(z, k, w),4585),Leaf(y,4725),List
                                                  //| (z, k, w, y),9310),Leaf(h,11298),List(z, k, w, y, h),20608),Leaf(q,20889),L
                                                  //| ist(z, k, w, y, h, q),41497),List(x, j, f, z, k, w, y, h, q),72127),List(d,
                                                  //|  x, j, f, z, k, w, y, h, q),128396),List(s, d, x, j, f, z, k, w, y, h, q),2
                                                  //| 50291),Fork(Fork(Leaf(o,82762),Leaf(l,83668),List(o, l),166430),Fork(Fork(L
                                                  //| eaf(m,45521),Leaf(p,46335),List(m, p),91856),Leaf(u,96785),List(m, p, u),18
                                                  //| 8641),List(o, l, m, p, u),355071),List(s, d, x, j, f, z, k, w, y, h, q, o, 
                                                  //| l, m, p, u),605362),Fork(Fork(Fork(Leaf(r,100500),Fork(Leaf(c,50003),Fork(L
                                                  //| eaf(v,24975),Fork(Leaf(g,13288),Leaf(b,13822),List(g, b),27110),List(v, g, 
                                                  //| b),52085),List(c, v, g,
                                                  //| Output exceeds cutoff limit.


  val secret: List[Bit] = List(0,0,1,1,1,0,1,0,1,1,1,0,0,1,1,0,1,0,0,1,1,0,1,0,1,1,0,0,1,1,1,1,1,0,1,0,1,1,0,0,0,0,1,0,1,1,1,0,0,1,0,0,1,0,0,0,1,0,0,0,1,0,1)
                                                  //> secret  : List[patmat.tmp.Bit] = List(0, 0, 1, 1, 1, 0, 1, 0, 1, 1, 1, 0, 0
                                                  //| , 1, 1, 0, 1, 0, 0, 1, 1, 0, 1, 0, 1, 1, 0, 0, 1, 1, 1, 1, 1, 0, 1, 0, 1, 1
                                                  //| , 0, 0, 0, 0, 1, 0, 1, 1, 1, 0, 0, 1, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 1
                                                  //| )

  val dedecodedSecret = decode(frenchCode, secret)//> dedecodedSecret  : List[Char] = List(h, u, f, f, m, a, n, e, s, t, c, o, o,
                                                  //|  l)
  
  t1                                              //> res0: patmat.Huffman.Fork = Fork(Leaf(a,2),Leaf(b,3),List(a, b),5)
  t1.left                                         //> res1: patmat.Huffman.CodeTree = Leaf(a,2)
  t1.right                                        //> res2: patmat.Huffman.CodeTree = Leaf(b,3)
  t2                                              //> res3: patmat.Huffman.Fork = Fork(Fork(Leaf(a,2),Leaf(b,3),List(a, b),5),Lea
                                                  //| f(d,4),List(a, b, d),9)
  t2.left                                         //> res4: patmat.Huffman.CodeTree = Fork(Leaf(a,2),Leaf(b,3),List(a, b),5)
  t2.right                                        //> res5: patmat.Huffman.CodeTree = Leaf(d,4)
	val a = new Leaf('a',2)                   //> a  : patmat.Huffman.Leaf = Leaf(a,2)
  //val a_fnc = isLeaf(a)
  val b = new Leaf('b',4)                         //> b  : patmat.Huffman.Leaf = Leaf(b,4)
  val LeafList = List(a,b)                        //> LeafList  : List[patmat.Huffman.Leaf] = List(Leaf(a,2), Leaf(b,4))
  weight(b)                                       //> res6: Int = 4
  def wgt_thresh(L: Leaf): Boolean = {weight(L) > 2}
                                                  //> wgt_thresh: (L: patmat.Huffman.Leaf)Boolean
  val f_L = LeafList.filter(wgt_thresh)           //> f_L  : List[patmat.Huffman.Leaf] = List(Leaf(b,4))
  a.weight                                        //> res7: Int = 2
  t1.weight                                       //> res8: Int = 5
  t2.weight                                       //> res9: Int = 9
  val weight_t1 = weight(t1)                      //> weight_t1  : Int = 5
  val wt_a = weight(a)                            //> wt_a  : Int = 2
  chars(a)                                        //> res10: List[Char] = List(a)
  chars(t1)                                       //> res11: List[Char] = List(a, b)
  chars(t2).contains('e')                         //> res12: Boolean = false
  val L = string2Chars("abcd")                    //> L  : List[Char] = List(a, b, c, d)
  //val B
  
  def times(chars: List[Char]): List[(Char, Int)] = {
      def incr(cc: Char, LL: List[(Char,Int)]): List[(Char,Int)] = {
   		//if LL contains the char cc then add 1 to its second parameter
   		//else concatenate (cc,1) to LL
   		val ccInList = LL.filter(_._1 == cc)
   		if (ccInList.isEmpty) {
   			LL:+(cc,1)
   		}
   		else {
   			val idx = LL.indexOf(ccInList(0))
   			LL.updated(idx, (cc,ccInList(0)._2 + 1))
   		}
      }
      def timesAcc(cc: List[Char], LL: List[(Char,Int)]): List[(Char,Int)] = {
        if(cc.isEmpty) LL
        else {
          timesAcc(cc.tail,incr(cc.head,LL))
        }
      }
      timesAcc(chars,Nil)
  }                                               //> times: (chars: List[Char])List[(Char, Int)]
  
  def makeOrderedLeafList(freqs: List[(Char, Int)]): List[Leaf] = {
    freqs.map(x => Leaf(x._1,x._2)).sortBy(weight)
  }                                               //> makeOrderedLeafList: (freqs: List[(Char, Int)])List[patmat.Huffman.Leaf]

  makeOrderedLeafList(List(('t', 2), ('e', 1), ('x', 3))) == List(Leaf('e',1), Leaf('t',2), Leaf('x',3))
                                                  //> res13: Boolean = true
  

  /**
   * Checks whether the list `trees` contains only one single code tree.
   */
  def singleton(trees: List[CodeTree]): Boolean = {
    if (trees.count(x => true) == 1) {true}
    else {false}
  }                                               //> singleton: (trees: List[patmat.Huffman.CodeTree])Boolean
  
  def makeCodeTree(left: CodeTree, right: CodeTree) =
    Fork(left, right, chars(left) ::: chars(right), weight(left) + weight(right))
                                                  //> makeCodeTree: (left: patmat.Huffman.CodeTree, right: patmat.Huffman.CodeTre
                                                  //| e)patmat.Huffman.Fork
  
  
  def combine(trees: List[CodeTree]): List[CodeTree] = {
    if(singleton(trees)) trees
    else {
      val t0 = trees(0)
      val t1 = trees(1)
      val wgt = weight(t0)+weight(t1)
      val T =  Fork(t0,t1,chars(t0)++chars(t1),wgt)
      return (trees.drop(2) :+ T).sortBy(weight)
      //val TL = (trees.drop(2) :+ T).sortBy(weight)
      //combine(TL)
    }
  }                                               //> combine: (trees: List[patmat.Huffman.CodeTree])List[patmat.Huffman.CodeTree
                                                  //| ]
  val leaflist = List(Leaf('e', 1), Leaf('t', 2), Leaf('x', 4))
                                                  //> leaflist  : List[patmat.Huffman.Leaf] = List(Leaf(e,1), Leaf(t,2), Leaf(x,4
                                                  //| ))
  combine(leaflist) == List(Fork(Leaf('e',1),Leaf('t',2),List('e', 't'),3), Leaf('x',4))
                                                  //> res14: Boolean = true
  
  
  def until(s: List[CodeTree] => Boolean, c: List[CodeTree] => List[CodeTree])(trees: List[CodeTree]): List[CodeTree] = {
    if (s(trees)) trees
    else {
      until(s,c)(c(trees))
    }
  }                                               //> until: (s: List[patmat.Huffman.CodeTree] => Boolean, c: List[patmat.Huffman
                                                  //| .CodeTree] => List[patmat.Huffman.CodeTree])(trees: List[patmat.Huffman.Cod
                                                  //| eTree])List[patmat.Huffman.CodeTree]
  val until_out = until(singleton,combine)(combine(leaflist))
                                                  //> until_out  : List[patmat.Huffman.CodeTree] = List(Fork(Fork(Leaf(e,1),Leaf(
                                                  //| t,2),List(e, t),3),Leaf(x,4),List(e, t, x),7))
  
  def createCodeTree(chars: List[Char]): CodeTree = {
    val L = makeOrderedLeafList(times(chars))
    val ListOut = until(singleton,combine)(L)
    ListOut(0)
  }                                               //> createCodeTree: (chars: List[Char])patmat.Huffman.CodeTree
  val createCodeTree_out = createCodeTree(L)      //> createCodeTree_out  : patmat.Huffman.CodeTree = Fork(Fork(Leaf(a,1),Leaf(b,
                                                  //| 1),List(a, b),2),Fork(Leaf(c,1),Leaf(d,1),List(c, d),2),List(a, b, c, d),4)
                                                  //| 
  
  type Bit = Int
   def encode_char(tree: CodeTree, cc: Char, BL: List[Bit]): List[Bit] = tree match {
      case Leaf(c,w) => BL
      case Fork(l,r,c,w) => {
        if (chars(l).contains(cc)) encode_char(l,cc,BL:+0)
        else if (chars(r).contains(cc)) encode_char(r,cc, BL:+1)
        else BL // you should never get here.
      }
    }                                             //> encode_char: (tree: patmat.Huffman.CodeTree, cc: Char, BL: List[patmat.tmp.
                                                  //| Bit])List[patmat.tmp.Bit]
  
  def encode(tree: CodeTree)(text: List[Char]): List[Bit] = {
    text.map(c => encode_char(tree,c,Nil)).flatten
  }                                               //> encode: (tree: patmat.Huffman.CodeTree)(text: List[Char])List[patmat.tmp.Bi
                                                  //| t]

  val BL = encode_char(frenchCode,'h',Nil)        //> BL  : List[patmat.tmp.Bit] = List(0, 0, 1, 1, 1, 0, 1)
  val secret_bits = dedecodedSecret.map(c => encode_char(frenchCode,c,Nil)).flatten
                                                  //> secret_bits  : List[patmat.tmp.Bit] = List(0, 0, 1, 1, 1, 0, 1, 0, 1, 1, 1,
                                                  //|  0, 0, 1, 1, 0, 1, 0, 0, 1, 1, 0, 1, 0, 1, 1, 0, 0, 1, 1, 1, 1, 1, 0, 1, 0,
                                                  //|  1, 1, 0, 0, 0, 0, 1, 0, 1, 1, 1, 0, 0, 1, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1,
                                                  //|  0, 1)
  secret_bits == secret                           //> res15: Boolean = true
  
  val tmp = decodedSecret zip dedecodedSecret.map(c => encode_char(frenchCode,c,Nil))
                                                  //> tmp  : List[(Char, List[patmat.tmp.Bit])] = List((h,List(0, 0, 1, 1, 1, 0, 
                                                  //| 1)), (u,List(0, 1, 1, 1)), (f,List(0, 0, 1, 1, 0, 1)), (f,List(0, 0, 1, 1, 
                                                  //| 0, 1)), (m,List(0, 1, 1, 0, 0)), (a,List(1, 1, 1, 1)), (n,List(1, 0, 1, 0))
                                                  //| , (e,List(1, 1, 0)), (s,List(0, 0, 0)), (t,List(1, 0, 1, 1)), (c,List(1, 0,
                                                  //|  0, 1, 0)), (o,List(0, 1, 0, 0)), (o,List(0, 1, 0, 0)), (l,List(0, 1, 0, 1)
                                                  //| ))
  
  type CodeTable = List[(Char, List[Bit])]

  /**
   * This function returns the bit sequence that represents the character `char` in
   * the code table `table`.
   */
  def codeBits(table: CodeTable)(char: Char): List[Bit] = {
    table.find(x=>(x._1 == char)).toList(0)._2
  }                                               //> codeBits: (table: patmat.tmp.CodeTable)(char: Char)List[patmat.tmp.Bit]
    

  /**
   * Given a code tree, create a code table which contains, for every character in the
   * code tree, the sequence of bits representing that character.
   *
   * Hint: think of a recursive solution: every sub-tree of the code tree `tree` is itself
   * a valid code tree that can be represented as a code table. Using the code tables of the
   * sub-trees, think of how to build the code table for the entire tree.
   */
  def convert(tree: CodeTree): CodeTable = {
    chars(tree) zip chars(tree).map(c => encode_char(tree,c,Nil))
  }                                               //> convert: (tree: patmat.Huffman.CodeTree)patmat.tmp.CodeTable
  
  val CT = convert(frenchCode)                    //> CT  : patmat.tmp.CodeTable = List((s,List(0, 0, 0)), (d,List(0, 0, 1, 0)), 
                                                  //| (x,List(0, 0, 1, 1, 0, 0, 0)), (j,List(0, 0, 1, 1, 0, 0, 1)), (f,List(0, 0,
                                                  //|  1, 1, 0, 1)), (z,List(0, 0, 1, 1, 1, 0, 0, 0, 0)), (k,List(0, 0, 1, 1, 1, 
                                                  //| 0, 0, 0, 1, 0)), (w,List(0, 0, 1, 1, 1, 0, 0, 0, 1, 1)), (y,List(0, 0, 1, 1
                                                  //| , 1, 0, 0, 1)), (h,List(0, 0, 1, 1, 1, 0, 1)), (q,List(0, 0, 1, 1, 1, 1)), 
                                                  //| (o,List(0, 1, 0, 0)), (l,List(0, 1, 0, 1)), (m,List(0, 1, 1, 0, 0)), (p,Lis
                                                  //| t(0, 1, 1, 0, 1)), (u,List(0, 1, 1, 1)), (r,List(1, 0, 0, 0)), (c,List(1, 0
                                                  //| , 0, 1, 0)), (v,List(1, 0, 0, 1, 1, 0)), (g,List(1, 0, 0, 1, 1, 1, 0)), (b,
                                                  //| List(1, 0, 0, 1, 1, 1, 1)), (n,List(1, 0, 1, 0)), (t,List(1, 0, 1, 1)), (e,
                                                  //| List(1, 1, 0)), (i,List(1, 1, 1, 0)), (a,List(1, 1, 1, 1)))
  val codeBits_out = codeBits(CT)('s')            //> codeBits_out  : List[patmat.tmp.Bit] = List(0, 0, 0)
  
    def mergeCodeTables(a: CodeTable, b: CodeTable): CodeTable = {
	  if (a.isEmpty) b
	  else {
		  val hd = a.head
		  // find the index where b has hd
		  val idx = b.indexWhere(x => (x._1 == hd._1))
		  if (idx == -1) mergeCodeTables(a.tail,b:+hd)
		  else {
		    if ((hd._2).length < (b(idx)._2).length) mergeCodeTables(a.tail,b)
		    else mergeCodeTables(a.tail,b.updated(idx,hd))
		  }
	  }
  }                                               //> mergeCodeTables: (a: patmat.tmp.CodeTable, b: patmat.tmp.CodeTable)patmat.t
                                                  //| mp.CodeTable
  CT.length                                       //> res16: Int = 26
  val ct_merged = mergeCodeTables(CT,CT)          //> ct_merged  : patmat.tmp.CodeTable = List((s,List(0, 0, 0)), (d,List(0, 0, 1
                                                  //| , 0)), (x,List(0, 0, 1, 1, 0, 0, 0)), (j,List(0, 0, 1, 1, 0, 0, 1)), (f,Lis
                                                  //| t(0, 0, 1, 1, 0, 1)), (z,List(0, 0, 1, 1, 1, 0, 0, 0, 0)), (k,List(0, 0, 1,
                                                  //|  1, 1, 0, 0, 0, 1, 0)), (w,List(0, 0, 1, 1, 1, 0, 0, 0, 1, 1)), (y,List(0, 
                                                  //| 0, 1, 1, 1, 0, 0, 1)), (h,List(0, 0, 1, 1, 1, 0, 1)), (q,List(0, 0, 1, 1, 1
                                                  //| , 1)), (o,List(0, 1, 0, 0)), (l,List(0, 1, 0, 1)), (m,List(0, 1, 1, 0, 0)),
                                                  //|  (p,List(0, 1, 1, 0, 1)), (u,List(0, 1, 1, 1)), (r,List(1, 0, 0, 0)), (c,Li
                                                  //| st(1, 0, 0, 1, 0)), (v,List(1, 0, 0, 1, 1, 0)), (g,List(1, 0, 0, 1, 1, 1, 0
                                                  //| )), (b,List(1, 0, 0, 1, 1, 1, 1)), (n,List(1, 0, 1, 0)), (t,List(1, 0, 1, 1
                                                  //| )), (e,List(1, 1, 0)), (i,List(1, 1, 1, 0)), (a,List(1, 1, 1, 1)))
  ct_merged.length                                //> res17: Int = 26
  
	singleton(List(a))                        //> res18: Boolean = true
	singleton(List(a,t1))                     //> res19: Boolean = false
  Nil == List()                                   //> res20: Boolean = true
  val p: (Char, Int) = ('c', 1)                   //> p  : (Char, Int) = (c,1)
  val q = ('d',4)                                 //> q  : (Char, Int) = (d,4)
  p._1                                            //> res21: Char = c
  p._2                                            //> res22: Int = 1
  val w = List(p)                                 //> w  : List[(Char, Int)] = List((c,1))
 // val b = w:+qs
  /*
  def incr(cc: Char, LL: List[(Char,Int)]): List[(Char,Int)] = {
   		//if LL contains the char cc then add 1 to its second parameter
   		//else concatenate (cc,1) to LL
   		val ccInList = LL.filter(_._1 == cc)
   		if (ccInList.isEmpty) {
   			LL:+(cc,1)
   		}
   		else {
   			val idx = LL.indexOf(ccInList(0))
   			LL.updated(idx, (cc,ccInList(0)._2 + 1))
   		}
  }
  
  var cda = incr('a',b)
  val T = incr('z',cda)
  incr('z',T)
    //val tmp = cda.find(ff)
  cda.filter(_._1 == 'c')
  cda.updated(2,(p._1,p._2 + 1))
  cda.indexOf('c')
  */
  

  
  val LL = times(L)                               //> LL  : List[(Char, Int)] = List((a,1), (b,1), (c,1), (d,1))

  // B = LL.sortBy(_._2)
  //LL.foreach(f(x: (Char,Int)) => Leaf(x._1,x._2))
  
  val C = LL.map(x => Leaf(x._1,x._2)).sortBy(weight)
                                                  //> C  : List[patmat.Huffman.Leaf] = List(Leaf(a,1), Leaf(b,1), Leaf(c,1), Leaf
                                                  //| (d,1))
  //val D = makeOrderedLeafList(LL)
  val T = makeOrderedLeafList(LL)                 //> T  : List[patmat.Huffman.Leaf] = List(Leaf(a,1), Leaf(b,1), Leaf(c,1), Leaf
                                                  //| (d,1))
  val R = (T:+a).sortBy(weight)                   //> R  : List[patmat.Huffman.Leaf] = List(Leaf(a,1), Leaf(b,1), Leaf(c,1), Leaf
                                                  //| (d,1), Leaf(a,2))
  val TT = combine(T)                             //> TT  : List[patmat.Huffman.CodeTree] = List(Leaf(c,1), Leaf(d,1), Fork(Leaf(
                                                  //| a,1),Leaf(b,1),List(a, b),2))
  val TTT = combine(TT)                           //> TTT  : List[patmat.Huffman.CodeTree] = List(Fork(Leaf(a,1),Leaf(b,1),List(a
                                                  //| , b),2), Fork(Leaf(c,1),Leaf(d,1),List(c, d),2))
  val TTTT = combine(TTT)                         //> TTTT  : List[patmat.Huffman.CodeTree] = List(Fork(Fork(Leaf(a,1),Leaf(b,1),
                                                  //| List(a, b),2),Fork(Leaf(c,1),Leaf(d,1),List(c, d),2),List(a, b, c, d),4))
  singleton(TTTT)                                 //> res23: Boolean = true
  singleton(Nil)                                  //> res24: Boolean = false
  combine(Nil)                                    //> java.lang.IndexOutOfBoundsException: 0
                                                  //| 	at scala.collection.LinearSeqOptimized$class.apply(LinearSeqOptimized.sc
                                                  //| ala:51)
                                                  //| 	at scala.collection.immutable.List.apply(List.scala:83)
                                                  //| 	at patmat.tmp$$anonfun$main$1.patmat$tmp$$anonfun$$combine$1(patmat.tmp.
                                                  //| scala:86)
                                                  //| 	at patmat.tmp$$anonfun$main$1.apply$mcV$sp(patmat.tmp.scala:225)
                                                  //| 	at org.scalaide.worksheet.runtime.library.WorksheetSupport$$anonfun$$exe
                                                  //| cute$1.apply$mcV$sp(WorksheetSupport.scala:76)
                                                  //| 	at org.scalaide.worksheet.runtime.library.WorksheetSupport$.redirected(W
                                                  //| orksheetSupport.scala:65)
                                                  //| 	at org.scalaide.worksheet.runtime.library.WorksheetSupport$.$execute(Wor
                                                  //| ksheetSupport.scala:75)
                                                  //| 	at patmat.tmp$.main(patmat.tmp.scala:4)
                                                  //| 	at patmat.tmp.main(patmat.tmp.scala)
  //val r = T.zipWithIndex.find(x=>weight(x._1)>1).map(x=>toInt(x._2))
  //val r = T.zipWithIndex.find(x=>weight(x._1)>1)
	val r = T.indexWhere(x=>(weight(x) > 1))
	
  val rr = T.find(x=>weight(x)>1)
  val rrIdx = T.indexOf(rr)
  T(3)
  val bb = r
  r==None
  r==3
	val ls = List("Mary", "had", "a", "little", "lamb","a")
	val ls_list = ls.zipWithIndex.filter(_._1 == "a").map(_._2)
    ls_list(0)
  //val ridx = T.indexOf(T==r)
  //val r = T.indexOf(T.find(x=>weight(x)>2))
  //val rr = T.find(x=>weight(x)>2)
  def f(cc: Char, p: (Char,Int)): Boolean = { p._1 == cc }
  f('c',p)
  f('c',q)
  
  def ff(p: (Char,Int)): Boolean = { p._1 == 'd' }
  

}