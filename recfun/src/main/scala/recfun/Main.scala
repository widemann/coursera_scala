package recfun
import common._

object Main {
  def main(args: Array[String]) {
    println("Pascal's Triangle")
    for (row <- 0 to 10) {
      for (col <- 0 to row)
        print(pascal(col, row) + " ")
      println()
    }
  }

  /**
   * Exercise 1
   */
  def pascal(c: Int, r: Int): Int = {
    if ((r == 0) || (c == 0) || (c == r))
    	1
    else
    	pascal(c-1,r-1) + pascal(c,r-1)
  }

  /**
   * Exercise 2
   */
  def balance(chars: List[Char]): Boolean = {
    
    def paren_cnt(s: List[Char],cnt: Int): Int = {
    	if (s.isEmpty || cnt < 0) {cnt}
    	else {
    		val h = s.head
    		if (h == '(') {
    			paren_cnt(s.tail,cnt+1)
    		}
    		else if (h == ')') {
    			paren_cnt(s.tail,cnt-1)
    		}
    		else {
    				paren_cnt(s.tail,cnt)
    		}
    	}
    }
    
    val cnt = paren_cnt(chars,0)
    if (cnt != 0) {false}
    else {true}
    
  }

  
  /**
   * Exercise 3
   */
  def countChange(money: Int, coins: List[Int]): Int = {
	  var cs = coins.distinct.sortWith(_ > _)
	  def count(n: Int, m: List[Int]): Int = {
		  if (n == 0){
			  1
		  }
		  else if (n < 0){
		  	  0
		  }
		  else if (n >= 1 && m.isEmpty){
			  0
		  }
		  else {
		  	  count(n,m.tail) + count(n-m.head,m)
		  }
	  }
	  count(money,cs)
  }
}
